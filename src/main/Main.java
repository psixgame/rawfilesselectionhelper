package main;

import main.file_processor.FileProcessor;
import main.utils.StatusMessagesHandler;

import java.awt.*;

public class Main {

    public static void main(final String[] args) {
        //create components
        final GuiPresenter guiPresenter = new GuiPresenter();
        final Gui gui = new Gui();
        final FileProcessor fileProcessor = new FileProcessor();
        final StatusMessagesHandler statusMessagesHandler = new StatusMessagesHandler();

        //setup dependencies
        gui.setGuiPresenter(guiPresenter);
        guiPresenter.setGui(gui);
        guiPresenter.setFileProcessor(fileProcessor);
        guiPresenter.setStatusMessagesHandler(statusMessagesHandler);
        fileProcessor.setStatusMessagesHandler(statusMessagesHandler);

        //init components
        guiPresenter.init();

        final Runnable runnable = gui::createWindow;
        EventQueue.invokeLater(runnable);
    }
}
