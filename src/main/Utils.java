package main;

import com.sun.istack.internal.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

public abstract class Utils {

    private Utils() {
    }

    private static final String FILE_EXTENSION_JPG = "JPG";
    private static final String FILE_EXTENSION_CR2 = "CR2";

    public static File getFile(final String path) {
        return new File(path);
    }

    public static List<File> getJpgFilesFromDirectory(final File directory) {
        assert directory.isDirectory();

        final List<File> result = new ArrayList<>();
        final File[] files = directory.listFiles();

        for (final File file : files) {
            if (isFileWithExtension(file, FILE_EXTENSION_JPG)) {
                result.add(file);
            }
        }

        return result;
    }

    private static boolean isFileWithExtension(final File file, final String extension) {
        return file.isFile() && fileNameHasExtension(file.getName(), extension);
    }

    private static boolean fileNameHasExtension(final String fileName, final String extension) {
        return fileName.toLowerCase().endsWith(extension.toLowerCase());
    }

    @Nullable
    public static File getCr2FileWithSameName(final File cr2Folder, final File jpgFile) {
        final String jpgFileNameWithoutExtension = getFileNameWithoutExtension(jpgFile);

        for (final File file : cr2Folder.listFiles()) {
            if (isFileWithExtension(file, FILE_EXTENSION_CR2)) {
                final String cr2FileNameWithoutExtension = getFileNameWithoutExtension(file);
                if (jpgFileNameWithoutExtension.equalsIgnoreCase(cr2FileNameWithoutExtension)) {
                    return file;
                }
            }
        }
        return null;
    }

    private static String getFileNameWithoutExtension(final File file) {
        final String fullName = file.getName();
        return fullName.substring(0, fullName.lastIndexOf("."));

    }

    public static void copyFileToFolder(final File file, final File folder) {
        final File destinationFile = new File(folder.getName() + File.separator + file.getName());
        try {
            Files.copy(Paths.get(file.getPath()), Paths.get(destinationFile.getPath()),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (final IOException e) {
            e.printStackTrace();
            Logger.log("Error: failed to copy files, cause=" + e.getMessage());
        }
    }
}
