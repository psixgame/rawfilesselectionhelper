package main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

final class Gui {

    private GuiPresenter guiPresenter;

    final void setGuiPresenter(GuiPresenter guiPresenter) {
        this.guiPresenter = guiPresenter;
    }

    private JFrame frame;

    final void createWindow() {
        //Create and set up the window
        frame = createFrame();

        final Container container = frame.getContentPane();
        container.setLayout(new GridBagLayout());
        final GridBagConstraints constraints = new GridBagConstraints();

        addChooseJpgButton(container, constraints);
        addJpgFolderPathTextField(container, constraints);
        addChooseRawButton(container, constraints);
        addRawFolderPathTextField(container, constraints);
        addGoButton(container, constraints);
        addLogTextArea(container, constraints);
    }

    private static final String FRAME_TITLE = "Copy raw files";

    private JFrame createFrame() {
        final JFrame frame = new JFrame(FRAME_TITLE);
        frame.setVisible(true);
        frame.setSize(600, 300);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        return frame;
    }

    private void addChooseJpgButton(final Container container, final GridBagConstraints constraints) {
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 0.0;
        constraints.weighty = 0.0;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(5, 5, 5, 5);

        final JButton chooseJpgBtn = new JButton(GuiConstants.CHOOSE_JPG_FOLDER);
        chooseJpgBtn.addActionListener(chooseJpgBtnActionListener);
        container.add(chooseJpgBtn, constraints);
    }

    private final ActionListener chooseJpgBtnActionListener = e -> guiPresenter.onClickBtnChooseJpgFolder(frame);

    private JTextField jpgFolderPathTextField;

    final String getJpgFolderPath() {
        return jpgFolderPathTextField.getText();
    }

    final void setJpgFolderPath(final String path) {
        jpgFolderPathTextField.setText(path);
    }

    private void addJpgFolderPathTextField(final Container container, final GridBagConstraints constraints) {
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1;
        constraints.weighty = 0.0;
        constraints.gridx = 1;
        constraints.gridy = 0;

        jpgFolderPathTextField = new JTextField();
        container.add(jpgFolderPathTextField, constraints);
    }

    private void addChooseRawButton(final Container container, final GridBagConstraints constraints) {
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 0.0;
        constraints.weighty = 0.0;
        constraints.gridx = 0;
        constraints.gridy = 1;

        final JButton chooseRawBtn = new JButton(GuiConstants.CHOOSE_RAW_FOLDER);
        chooseRawBtn.addActionListener(chooseRawBtnActionListener);
        container.add(chooseRawBtn, constraints);
    }

    private final ActionListener chooseRawBtnActionListener = e -> guiPresenter.onClickBtnChooseRawFolder(frame);

    private JTextField rawFolderPathTextField;

    final String getRawFolderPath() {
        return rawFolderPathTextField.getText();
    }

    final void setRawFolderPath(final String path) {
        rawFolderPathTextField.setText(path);
    }

    private void addRawFolderPathTextField(final Container container, final GridBagConstraints constraints) {
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1;
        constraints.gridx = 1;
        constraints.gridy = 1;

        rawFolderPathTextField = new JTextField();
        container.add(rawFolderPathTextField, constraints);
    }

    private static final String BTN_GO_LABEL = "Go";

    private void addGoButton(final Container container, final GridBagConstraints constraints) {
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 0.0;
        constraints.weighty = 0.0;
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 1;

        final JButton goBtn = new JButton(BTN_GO_LABEL);
        goBtn.addActionListener(goBtnActionListener);
        container.add(goBtn, constraints);
    }

    private final ActionListener goBtnActionListener = e -> guiPresenter.onClickBtnGo();

    private JTextArea logTextArea;

    final void appendStatusMessage(final String text) {
        if (!logTextArea.getText().equals("")) {
            logTextArea.append("\n");
        }
        logTextArea.append(text);
    }

    private void addLogTextArea(final Container container, final GridBagConstraints constraints) {
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1;
        constraints.weighty = 1;
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 2;

        logTextArea = new JTextArea();
        final JScrollPane logTextAreaScrollPane = new JScrollPane(logTextArea);
        container.add(logTextAreaScrollPane, constraints);
    }

}
