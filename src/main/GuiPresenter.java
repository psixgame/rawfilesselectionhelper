package main;

import main.file_processor.FileProcessor;
import main.utils.StatusMessagesHandler;

import javax.swing.*;
import java.io.File;

final class GuiPresenter {

    final void init() {
        statusMessagesHandler.setStatusMessageListener(this::handleStatusMessage);
    }

    final void onClickBtnChooseJpgFolder(final JFrame frame) {
        final JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File("."));
        fileChooser.setDialogTitle(GuiConstants.CHOOSE_JPG_FOLDER);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setAcceptAllFileFilterUsed(false);

        handleChooseJpgFolderResult(fileChooser, fileChooser.showOpenDialog(frame));
    }

    private void handleChooseJpgFolderResult(final JFileChooser fileChooser, final int result) {
        if (JFileChooser.APPROVE_OPTION != result) return;
        gui.setJpgFolderPath(fileChooser.getSelectedFile().getPath());
    }

    final void onClickBtnChooseRawFolder(final JFrame frame) {
        final JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File("."));
        fileChooser.setDialogTitle(GuiConstants.CHOOSE_RAW_FOLDER);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setAcceptAllFileFilterUsed(false);

        handleChooseRawFolderResult(fileChooser, fileChooser.showOpenDialog(frame));
    }

    private void handleChooseRawFolderResult(final JFileChooser fileChooser, final int result) {
        if (JFileChooser.APPROVE_OPTION != result) return;
        gui.setRawFolderPath(fileChooser.getSelectedFile().getPath());
    }

    final void onClickBtnGo() {
        final String jpgPath = gui.getJpgFolderPath();
        final String rawPath = gui.getRawFolderPath();

        final String[] args = new String[]{jpgPath, rawPath};

        fileProcessor.processArguments(args);
    }

    private void handleStatusMessage(final String message) {
        gui.appendStatusMessage(message);
    }

    private Gui gui;

    final void setGui(Gui gui) {
        this.gui = gui;
    }

    private FileProcessor fileProcessor;

    final void setFileProcessor(FileProcessor fileProcessor) {
        this.fileProcessor = fileProcessor;
    }

    private StatusMessagesHandler statusMessagesHandler;

    final void setStatusMessagesHandler(StatusMessagesHandler statusMessagesHandler) {
        this.statusMessagesHandler = statusMessagesHandler;
    }
}
