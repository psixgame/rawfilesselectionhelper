package main;

final class GuiConstants {

    private GuiConstants() {}

    static final String CHOOSE_JPG_FOLDER = "Choose jpg folder";
    static final String CHOOSE_RAW_FOLDER = "Choose raw folder";

}
