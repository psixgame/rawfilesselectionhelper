package main.utils;

public interface Action<T> {

    void run(T f);
}
