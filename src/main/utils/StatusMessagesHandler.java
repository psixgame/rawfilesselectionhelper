package main.utils;

public final class StatusMessagesHandler {

    private Action<String> statusMessageListener;

    public final void setStatusMessageListener(final Action<String> listener) {
        statusMessageListener = listener;
    }

    public final void postStatusMessage(final String message) {
        if (hasStatusMessageListener()) {
            statusMessageListener.run(message);
        }
    }

    private boolean hasStatusMessageListener() {
        return statusMessageListener != null;
    }

}
