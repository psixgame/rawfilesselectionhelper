package main.file_processor;

import main.Logger;
import main.Utils;
import main.utils.StatusMessagesHandler;

import java.io.File;
import java.util.List;

public final class FileProcessor {

    private static final String CR2_OUTPUT_FOLDER_NAME = "output";

    public final void processArguments(final String[] args) {
        if (!validateArguments(args)) {
            return;
        }

        if (!validateFolders(args)) {
            return;
        }

        processFolders(args);
    }

    private static final String ERR_NO_ARGUMENTS = "Error: there is no arguments";
    private static final String ERR_WRONG_ARGUMENT_COUNT = "Error: there should be 2 arguments: jpg folder path and cr2 folder path";
    private static final String ERR_FOLDER_NOT_EXIST = "Error: folder \"%s\" not exists";
    private static final String ERR_IS_NOT_A_FOLDER = "Error: \"%s\" is not a folder";
    private static final String ERR_FOLDER_IS_EMPTY = "Error: folder \"%s\" is empty";
    private static final String ERR_CANT_CREATE_OUTPUT_DIRECTORY = "Error: cant create output directory";
    private static final String ERR_NO_RAW_FILE_FOR = "Error: no RAW file for \"%s\"";

    private static final String MSG_OUTPUT_DIRECTORY_ALREADY_EXISTS = "output directory already exists";
    private static final String MSG_CREATED_OUTPUT_DIRECTORY = "Created output directory: %s";
    private static final String MSG_FILES_TO_PROCESS = "Files to process: %d";
    private static final String MSG_PROCESSING_FILE = "Processing file: %s";
    private static final String MSG_FINISHED = "Finished";

    private boolean validateArguments(final String[] args) {
        final boolean hasArguments = args != null;
        if (!hasArguments) {
            logAndPostStatusMessage(ERR_NO_ARGUMENTS);
            return false;
        }

        final boolean hasProperArgumentsCount = args.length == 2;
        if (!hasProperArgumentsCount) {
            logAndPostStatusMessage(ERR_WRONG_ARGUMENT_COUNT);
            return false;
        }

        return true;
    }

    private boolean validateFolders(final String[] args) {
        final String jpgFolderPath = args[0];
        if (!validateFolder(jpgFolderPath)) {
            return false;
        }

        final String cr2FolderPath = args[1];
        return validateFolder(cr2FolderPath);
    }

    private boolean validateFolder(final String path) {
        final File folder = Utils.getFile(path);

        final boolean folderExists = folder.exists();
        if (!folderExists) {
            logAndPostStatusMessage(String.format(ERR_FOLDER_NOT_EXIST, path));
            return false;
        }

        final boolean folderIsDirectory = folder.isDirectory();
        if (!folderIsDirectory) {
            logAndPostStatusMessage(String.format(ERR_IS_NOT_A_FOLDER, path));
            return false;
        }

        final File[] files = folder.listFiles();
        if (files == null || files.length == 0) {
            logAndPostStatusMessage(String.format(ERR_FOLDER_IS_EMPTY, path));
            return false;
        }

        return true;
    }

    private void processFolders(final String[] paths) {
        final File jpgFolder = Utils.getFile(paths[0]);
        final File cr2Folder = Utils.getFile(paths[1]);

        final List<File> jpgFiles = Utils.getJpgFilesFromDirectory(jpgFolder);

        logAndPostStatusMessage(String.format(MSG_FILES_TO_PROCESS, jpgFiles.size()));

        final File outputFolder = new File(CR2_OUTPUT_FOLDER_NAME);
        if (outputFolder.exists() && outputFolder.isDirectory()) {
            logAndPostStatusMessage(MSG_OUTPUT_DIRECTORY_ALREADY_EXISTS);
        } else if (!outputFolder.mkdir()) {
            logAndPostStatusMessage(ERR_CANT_CREATE_OUTPUT_DIRECTORY);
            return;
        } else {
            logAndPostStatusMessage(String.format(MSG_CREATED_OUTPUT_DIRECTORY, outputFolder.getAbsolutePath()));
        }

        for (final File jpgFile : jpgFiles) {
            logAndPostStatusMessage(String.format(MSG_PROCESSING_FILE, jpgFile.getName()));
            final File cr2File = Utils.getCr2FileWithSameName(cr2Folder, jpgFile);
            if (cr2File == null) {
                logAndPostStatusMessage(String.format(ERR_NO_RAW_FILE_FOR, jpgFile.getName()));
                continue;
            }
            Utils.copyFileToFolder(cr2File, outputFolder);
        }

        logAndPostStatusMessage(MSG_FINISHED);
    }

    private void logAndPostStatusMessage(final String message) {
        Logger.log(message);
        statusMessagesHandler.postStatusMessage(message);
    }

    private StatusMessagesHandler statusMessagesHandler;

    public final void setStatusMessagesHandler(StatusMessagesHandler statusMessagesHandler) {
        this.statusMessagesHandler = statusMessagesHandler;
    }

}
