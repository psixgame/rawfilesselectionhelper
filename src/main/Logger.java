package main;

public abstract class Logger {

    private Logger() {}

    public static void log(final String message) {
        System.out.println(message);
    }

}
